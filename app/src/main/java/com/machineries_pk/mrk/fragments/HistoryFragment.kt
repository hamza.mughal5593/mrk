package com.machineries_pk.mrk.fragments

import android.app.Dialog
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.Utils.MySingleton
import com.machineries_pk.mrk.Utils.Server
import com.machineries_pk.mrk.adapters.EmployeeRecyclerAdapter
import com.machineries_pk.mrk.adapters.HistoryRecyclerAdapter
import com.machineries_pk.mrk.databinding.FragmentHistoryBinding
import com.machineries_pk.mrk.models.EmployeeModel
import com.machineries_pk.mrk.models.HistoryModel
import io.paperdb.Paper
import org.json.JSONException
import org.json.JSONObject

class HistoryFragment : Fragment() {

    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentHistoryBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHistoryBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

       get()

    }

//    private val sampleList : ArrayList<HistoryModel>
       fun get()
        {
//            itemlist.add(HistoryModel(BitmapFactory.decodeResource(resources,R.drawable.mall_sample),"Mall 1",123456,"Code:1001","AED",5000,"12/12/12"))
//            itemlist.add(HistoryModel(BitmapFactory.decodeResource(resources,R.drawable.mall_sample),"Mall 2",123457,"Code:1002","AED",4000,"12/12/12"))
//            itemlist.add(HistoryModel(BitmapFactory.decodeResource(resources,R.drawable.mall_sample),"Mall 3",1234588,"Code:1021","AED",2000,"12/12/12"))
//            return itemlist



            val dialog = Dialog(requireContext())
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.loading_dialog)




            dialog.show()

            val RegistrationRequest: StringRequest = object : StringRequest(
                Method.GET,
                "http://vps-86b76da8.vps.ovh.ca:7070/api/Transaction/GetInvicesHistory?mobileNumber=+971585703053",
                Response.Listener
                { response ->
                    try {


                        val objects = JSONObject(response)
                        val response_code = objects.getInt("errorCode")
                        if (response_code == 0) {
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss()
                            }
                            val itemlist: ArrayList<HistoryModel> = ArrayList()
                            val data = objects.getJSONObject("data")
binding.totalSalesAmount.text = data.getInt("total").toString()
binding.dueBalanceAmount.text = data.getInt("due").toString()

                            val invoices = data.getJSONArray("invoices")
                            for (i in 0 until invoices.length()) {
                                val jsonObject = invoices.getJSONObject(i)

                                val whsName = jsonObject.getString("whsName")
                                val docTotal = jsonObject.getInt("docTotal")
                                val docDate = jsonObject.getString("docDate")
                                val docNum = jsonObject.getInt("docNum")

                                val separated: List<String> = docDate.split("T")



                                itemlist.add(HistoryModel(BitmapFactory.decodeResource(resources,R.drawable.mall_sample),whsName,docNum,"Code:1001","AED",docTotal,separated?.get(0)))



                            }
                            viewManager = LinearLayoutManager(requireContext())

                            viewAdapter = HistoryRecyclerAdapter(requireContext(),itemlist)

                            binding.recyclerview.adapter = viewAdapter

                            binding.recyclerview.apply{
                                // use this setting to improve performance if you know that changes
                                // in content do not change the layout size of the RecyclerView
                                setHasFixedSize(true)

                                // use a linear layout manager
                                layoutManager = viewManager

                                // specify an viewAdapter (see also next example)
                                adapter = viewAdapter

                            }                        } else {
                            Toast.makeText(
                                requireContext(),
                                objects.getString("errorMessage"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        //                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    dialog.dismiss()
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(volleyError: VolleyError) {
                        dialog.dismiss()
                        var message: String? = null
                        check_internet()
                        when (volleyError) {
                            is NetworkError -> {
                                message = "Cannot connect to Internet...Please check your connection!"
                            }
                            is ServerError -> {
                                message =
                                    "The server could not be found. Please try again after some time!!"
                            }
                            is AuthFailureError -> {
                                message = "Cannot connect to Internet...Please check your connection!"
                            }
                            is ParseError -> {
                                message = "Parsing error! Please try again after some time!!"
                            }
                            is NoConnectionError -> {
                                message = "Cannot connect to Internet...Please check your connection!"
                            }
                            is TimeoutError -> {
                                message = "Connection TimeOut! Please check your internet connection."
                            }
                        }
                        Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
                    }
                }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val header: MutableMap<String, String> = HashMap()
                    header["Authorization"] = "bearer ${Paper.book().read("token","")}"
//                header["x-rapidapi-host"] = "twinword-word-association-quiz.p.rapidapi.com"
                    return header


                }
            }
            RegistrationRequest.retryPolicy = DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            MySingleton.getInstance(requireContext()).addToRequestQueue(RegistrationRequest)





        }
    private fun check_internet() {
        val dialogexit = Dialog(requireContext())
        dialogexit.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogexit.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogexit.setCancelable(true)
        dialogexit.setContentView(R.layout.check_internet_dialog)
//        val retry = dialogexit.findViewById<View>(R.id.retry) as TextView
//        val exit = dialogexit.findViewById<View>(R.id.exit) as TextView
//        retry.setOnClickListener {
//            if (Utils.isInternetAvailable_for_popup(this@QuizQuestionsActivity)) {
//                dialogexit.dismiss()
//                finish()
//            } else {
//                Toast.makeText(
//                    this@QuizQuestionsActivity,
//                    "Internet not available",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//        exit.setOnClickListener {
//            dialogexit.dismiss()
//            finish()
//        }
        dialogexit.show()
    }
}