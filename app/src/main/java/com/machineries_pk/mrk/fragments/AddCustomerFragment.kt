package com.machineries_pk.mrk.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.databinding.FragmentAddCustomerBinding

class AddCustomerFragment : Fragment() {

    private lateinit var binding : FragmentAddCustomerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentAddCustomerBinding.inflate(layoutInflater,container,false)
        return inflater.inflate(R.layout.fragment_add_customer, container, false)
    }

}