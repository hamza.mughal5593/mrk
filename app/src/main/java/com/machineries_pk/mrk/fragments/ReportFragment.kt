package com.machineries_pk.mrk.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.adapters.AllItemsAdapter
import com.machineries_pk.mrk.databinding.FragmentReportBinding
import com.machineries_pk.mrk.models.EmployeeModel
import com.machineries_pk.mrk.models.ItemModel

class ReportFragment : Fragment() {

    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var binding: FragmentReportBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentReportBinding.inflate(layoutInflater,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewManager = LinearLayoutManager(requireContext())

        viewAdapter = AllItemsAdapter(requireContext(),sampleList)

        binding.recyclerview.adapter = viewAdapter

        binding.recyclerview.apply{
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }

    }

    private val sampleList : ArrayList<ItemModel>
        get()
        {
            val itemlist: ArrayList<ItemModel> = ArrayList()
            itemlist.add(
                ItemModel(BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
            "New Model","Oyster",500))
            itemlist.add(ItemModel(BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
                "New Model","Oyster",500))
            itemlist.add(ItemModel(BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
                "New Model","Oyster",500))
            return itemlist

        }

}