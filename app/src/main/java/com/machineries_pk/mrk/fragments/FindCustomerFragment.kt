package com.machineries_pk.mrk.fragments

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.Utils.MySingleton
import com.machineries_pk.mrk.Utils.Server
import com.machineries_pk.mrk.activities.CartActivity
import com.machineries_pk.mrk.activities.EmployeeSelection
import com.machineries_pk.mrk.databinding.FragmentFindCustomerBinding
import io.paperdb.Paper
import org.json.JSONException
import org.json.JSONObject

class FindCustomerFragment : Fragment() {

    private lateinit var binding: FragmentFindCustomerBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentFindCustomerBinding.inflate(layoutInflater,container,false)


        binding.submitBtn.setOnClickListener {
            if (binding.numberEdittext.text.isNullOrBlank() ){
                Toast.makeText(requireContext(),"Fill all fields",Toast.LENGTH_SHORT).show()
            }else{
                find_customer()
            }
        }


        return binding.root
    }

    private fun find_customer() {


        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loading_dialog)




        dialog.show()

        val RegistrationRequest: StringRequest = object : StringRequest(
            Method.GET,
            Server.baseurl +"GetCustomerName?mobileNumber=+971585703053",
            Response.Listener
            { response ->
                try {


                    val objects = JSONObject(response)
                    val response_code = objects.getInt("errorCode")
                    if (response_code == 0) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        val data = objects.getJSONObject("data")
                        val mobileNumber = data.getString("mobileNumber")
//                        Paper.book().write("token",token)
                        val name = data.getString("name")
                        if ( !Paper.book().read("from_invoice",true)){


                            val fm = requireActivity().supportFragmentManager
                            fm.beginTransaction().replace(binding.framelayout.id,HistoryFragment()).commit()
                        }else{
                                                    requireActivity().startActivity(Intent( requireActivity(), CartActivity::class.java))

                        }

                    } else {
                        Toast.makeText(
                            requireActivity(),
                            objects.getString("errorMessage"),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    //                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                dialog.dismiss()
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    dialog.dismiss()
                    var message: String? = null
                    check_internet()
                    when (volleyError) {
                        is NetworkError -> {
                            message = "Cannot connect to Internet...Please check your connection!"
                        }
                        is ServerError -> {
                            message =
                                "The server could not be found. Please try again after some time!!"
                        }
                        is AuthFailureError -> {
                            message = "Cannot connect to Internet...Please check your connection!"
                        }
                        is ParseError -> {
                            message = "Parsing error! Please try again after some time!!"
                        }
                        is NoConnectionError -> {
                            message = "Cannot connect to Internet...Please check your connection!"
                        }
                        is TimeoutError -> {
                            message = "Connection TimeOut! Please check your internet connection."
                        }
                    }
                    Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
                }
            }) {
            override fun getHeaders(): MutableMap<String, String> {
                val header: MutableMap<String, String> = HashMap()
                header["Authorization"] = "bearer ${Paper.book().read("token","")}"
//                header["x-rapidapi-host"] = "twinword-word-association-quiz.p.rapidapi.com"
                return header


            }
        }
        RegistrationRequest.retryPolicy = DefaultRetryPolicy(
            25000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        MySingleton.getInstance(requireContext()).addToRequestQueue(RegistrationRequest)
    }

    private fun check_internet() {
        val dialogexit = Dialog(requireContext())
        dialogexit.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogexit.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogexit.setCancelable(true)
        dialogexit.setContentView(R.layout.check_internet_dialog)
//        val retry = dialogexit.findViewById<View>(R.id.retry) as TextView
//        val exit = dialogexit.findViewById<View>(R.id.exit) as TextView
//        retry.setOnClickListener {
//            if (Utils.isInternetAvailable_for_popup(this@QuizQuestionsActivity)) {
//                dialogexit.dismiss()
//                finish()
//            } else {
//                Toast.makeText(
//                    this@QuizQuestionsActivity,
//                    "Internet not available",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//        exit.setOnClickListener {
//            dialogexit.dismiss()
//            finish()
//        }
        dialogexit.show()
    }

}