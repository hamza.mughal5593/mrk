package com.machineries_pk.mrk.adapters

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.activities.Home
import com.machineries_pk.mrk.activities.MainActivity
import com.machineries_pk.mrk.models.EmployeeModel
import java.io.ByteArrayOutputStream

class EmployeeRecyclerAdapter(
    private val context: Context,
    private val list: ArrayList<EmployeeModel>
) : RecyclerView.Adapter<EmployeeRecyclerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img: ImageView = itemView.findViewById(R.id.employee_img)
        var name: TextView = itemView.findViewById(R.id.employee_name)
        var role: TextView = itemView.findViewById(R.id.employee_role)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_employee, parent , false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = list[position].name
        holder.role.text = list[position].role
        holder.img.setImageBitmap(list[position].image)

        holder.itemView.setOnClickListener {
            val intent = Intent(context, Home::class.java)
            val stream = ByteArrayOutputStream()
            list[position].image.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val byteArray = stream.toByteArray()
//            intent.putExtra("image",byteArray)
//            intent.putExtra("name",list[position].name)
//            intent.putExtra("role",list[position].role)
            context.startActivity(intent)


        }

    }

    override fun getItemCount() = list.size

}