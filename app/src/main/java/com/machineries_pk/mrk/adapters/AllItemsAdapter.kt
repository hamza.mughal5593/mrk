package com.machineries_pk.mrk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.models.ItemModel

class AllItemsAdapter(
    private val context: Context,
    private val list: ArrayList<ItemModel>
    ) : RecyclerView.Adapter<AllItemsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img: ImageView = itemView.findViewById(R.id.item2_image)
        var name: TextView = itemView.findViewById(R.id.item2_name)
        var code: TextView = itemView.findViewById(R.id.item2_code)
        var info1: TextView = itemView.findViewById(R.id.item2_info1)
        var info2: TextView = itemView.findViewById(R.id.item2_info2)
        var price: TextView = itemView.findViewById(R.id.item2_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item2, parent , false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.img.setImageBitmap(list[position].image)
        holder.name.text = list[position].model
        holder.code.text = list[position].code.toString()
        holder.info1.text = list[position].info1
        holder.info2.text = list[position].info2
        holder.price.text = list[position].price.toString()




    }

    override fun getItemCount() = list.size
}