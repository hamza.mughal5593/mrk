package com.machineries_pk.mrk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.models.SelectedItemModel

class SelectedItemsAdapter(
    private val context: Context,
    private val list: ArrayList<SelectedItemModel>
) : RecyclerView.Adapter<SelectedItemsAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var img: ImageView = itemView.findViewById(R.id.selected_items_item_image)
        var model: TextView = itemView.findViewById(R.id.selected_items_item_model)
        var serial: TextView = itemView.findViewById(R.id.selected_items_item_serial)
        var price: TextView = itemView.findViewById(R.id.selected_items_item_price)
        var disc: TextView = itemView.findViewById(R.id.selected_items_item_discount)
        var total: TextView = itemView.findViewById(R.id.selected_items_item_total)
        var delete: ImageView = itemView.findViewById(R.id.selected_items_item_delete)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.selected_items_item, parent , false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.img.setImageBitmap(list[position].image)
        holder.model.text = list[position].model
        holder.serial.text = list[position].serial
        holder.price.text = list[position].price.toString()
        holder.disc.text = list[position].discount.toString()
        holder.total.text = list[position].total.toString()

        holder.delete.setOnClickListener {

        }

    }

    override fun getItemCount() = list.size
}