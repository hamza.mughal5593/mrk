package com.machineries_pk.mrk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.models.HistoryModel

class HistoryRecyclerAdapter(
    private val context: Context,
    private val list: ArrayList<HistoryModel>
): RecyclerView.Adapter<HistoryRecyclerAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img: ImageView = itemView.findViewById(R.id.history_item_image)
        var name: TextView = itemView.findViewById(R.id.history_item_name)
        var serial: TextView = itemView.findViewById(R.id.history_item_serial_number)
        var code: TextView = itemView.findViewById(R.id.history_item_code)
        var currency: TextView = itemView.findViewById(R.id.history_item_currency)
        var amount: TextView = itemView.findViewById(R.id.history_item_amount)
        var date: TextView = itemView.findViewById(R.id.history_item_date)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.history_item, parent , false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = list[position].name
        holder.img.setImageBitmap(list[position].image)
        holder.serial.text = list[position].serial.toString()
        holder.code.text = list[position].code
        holder.currency.text = list[position].currency
        holder.amount.text = list[position].amount.toString()
        holder.date.text = list[position].date
    }

    override fun getItemCount() = list.size
}