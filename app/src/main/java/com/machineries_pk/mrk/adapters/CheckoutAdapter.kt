package com.machineries_pk.mrk.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.models.CheckoutModel

class CheckoutAdapter(
    private val context: Context,
    private val list: ArrayList<CheckoutModel>
) : RecyclerView.Adapter<CheckoutAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        var count: TextView = itemView.findViewById(R.id.checkout_item_count)
        var img: ImageView = itemView.findViewById(R.id.checkout_item_image)
        var discription: TextView = itemView.findViewById(R.id.checkout_item_description)
        var price: TextView = itemView.findViewById(R.id.checkout_item_price)
        var vat: TextView = itemView.findViewById(R.id.checkout_item_vat)
        var disc: TextView = itemView.findViewById(R.id.checkout_item_discount)
        var total: TextView = itemView.findViewById(R.id.checkout_item_total)
        var delete: ImageView = itemView.findViewById(R.id.checkout_item_delete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.checkout_item, parent , false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.count.text = (position+1).toString()
        holder.img.setImageBitmap(list[position].image)
        holder.discription.text = list[position].description
        holder.price.text = list[position].price.toString()
        holder.vat.text = list[position].vat.toString()
        holder.disc.text = list[position].discount.toString()
        holder.total.text = list[position].total.toString()

        holder.delete.setOnClickListener {

        }

    }

    override fun getItemCount() = list.size
}