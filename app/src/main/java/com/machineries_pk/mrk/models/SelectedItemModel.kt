package com.machineries_pk.mrk.models

import android.graphics.Bitmap

data class SelectedItemModel(var image: Bitmap, var model: String, var serial: String,var price: Int, var discount: Int, var total: Int)
