package com.machineries_pk.mrk.models

import android.graphics.Bitmap

data class ItemModel(var image: Bitmap, var code: String, var model: String,var info1: String, var info2: String, var price: Int)