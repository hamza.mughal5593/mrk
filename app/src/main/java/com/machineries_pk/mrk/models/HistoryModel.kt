package com.machineries_pk.mrk.models

import android.graphics.Bitmap

data class HistoryModel(var image: Bitmap, var name: String, var serial: Int, var code: String, var currency: String, var amount: Int, var date: String)
