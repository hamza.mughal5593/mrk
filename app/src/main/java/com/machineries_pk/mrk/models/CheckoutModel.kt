package com.machineries_pk.mrk.models

import android.graphics.Bitmap

data class CheckoutModel(var image: Bitmap, var description: String, var price: Int, var vat: Int, var discount: Int, var qty: Int, var total: Int)
