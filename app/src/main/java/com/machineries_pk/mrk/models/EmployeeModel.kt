package com.machineries_pk.mrk.models

import android.graphics.Bitmap

data class EmployeeModel(var image: Bitmap, var name: String, var role: String)
