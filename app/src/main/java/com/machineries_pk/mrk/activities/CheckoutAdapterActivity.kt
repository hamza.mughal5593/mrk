package com.machineries_pk.mrk.activities

import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.adapters.AllItemsAdapter
import com.machineries_pk.mrk.adapters.CheckoutAdapter
import com.machineries_pk.mrk.databinding.ActivityCheckoutAdapterBinding
import com.machineries_pk.mrk.models.CheckoutModel
import com.machineries_pk.mrk.models.SelectedItemModel

class CheckoutAdapterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCheckoutAdapterBinding
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutAdapterBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewManager = LinearLayoutManager(this)

        viewAdapter = CheckoutAdapter(this,sampleList)

        binding.recyclerview.adapter = viewAdapter

        binding.recyclerview.apply{
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManager

            // specify an viewAdapter (see also next example)
            adapter = viewAdapter

        }



    }

    private val sampleList : ArrayList<CheckoutModel>
        get()
        {
            val itemList: ArrayList<CheckoutModel> = ArrayList()
            itemList.add(
                CheckoutModel(
                    BitmapFactory.decodeResource(resources, R.drawable.khan_image),"Model: 123123\nSerial: 123123",
                    3000,200,-500, 1,2500
                )
            )
            itemList.add(
                CheckoutModel(
                    BitmapFactory.decodeResource(resources, R.drawable.khan_image),"Model: 123123\nSerial: 123123",
                    3000,200,-500, 1,2500
                )
            )
            itemList.add(
                CheckoutModel(
                    BitmapFactory.decodeResource(resources, R.drawable.khan_image),"Model: 123123\nSerial: 123123",
                    3000,200,-500, 1,2500
                )
            )

            return itemList
        }

}