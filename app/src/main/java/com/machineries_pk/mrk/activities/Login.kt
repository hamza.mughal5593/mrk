package com.machineries_pk.mrk.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.Utils.MySingleton
import com.machineries_pk.mrk.Utils.Server
import com.machineries_pk.mrk.databinding.ActivityLoginBinding
import io.paperdb.Paper
import org.json.JSONException
import org.json.JSONObject

class Login : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.btn.setOnClickListener {
            if (binding.usernameEdittext.text.isNullOrBlank() || binding.passwordEdittext.text.isNullOrBlank()){
                Toast.makeText(this,"Fill all fields",Toast.LENGTH_SHORT).show()
            }else{
                login()
            }
        }

    }
    private fun login() {


        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.loading_dialog)




        dialog.show()

        val RegistrationRequest: StringRequest = object : StringRequest(
            Method.GET,
            Server.baseurl +"login?userName=${binding.usernameEdittext.text.toString()}&password=${binding.passwordEdittext.text.toString()}",
            Response.Listener
            { response ->
                try {


                    val objects = JSONObject(response)
                    val response_code = objects.getInt("errorCode")
                    if (response_code == 0) {
                        if (dialog != null && dialog.isShowing()) {
                            dialog.dismiss();
                        }
                        val data = objects.getJSONObject("data")
                        val token = data.getString("token")
                        Paper.book().write("token",token)
                        val whseName = data.getString("whseName")
                        startActivity(Intent(this, EmployeeSelection::class.java))

                    } else {
                        Toast.makeText(
                            this@Login,
                            objects.getString("errorMessage"),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    //                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                } catch (e: JSONException) {
                    e.printStackTrace()
                }
                dialog.dismiss()
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(volleyError: VolleyError) {
                    dialog.dismiss()
                    var message: String? = null
                    check_internet()
                    when (volleyError) {
                        is NetworkError -> {
                            message = "Cannot connect to Internet...Please check your connection!"
                        }
                        is ServerError -> {
                            message =
                                "The server could not be found. Please try again after some time!!"
                        }
                        is AuthFailureError -> {
                            message = "Cannot connect to Internet...Please check your connection!"
                        }
                        is ParseError -> {
                            message = "Parsing error! Please try again after some time!!"
                        }
                        is NoConnectionError -> {
                            message = "Cannot connect to Internet...Please check your connection!"
                        }
                        is TimeoutError -> {
                            message = "Connection TimeOut! Please check your internet connection."
                        }
                    }
                    Toast.makeText(this@Login, message, Toast.LENGTH_LONG).show()
                }
            }) {
//            override fun getHeaders(): MutableMap<String, String> {
//                val header: MutableMap<String, String> = HashMap()
//                header["x-rapidapi-key"] = apikey
//                header["x-rapidapi-host"] = "twinword-word-association-quiz.p.rapidapi.com"
//                return header
//
//
//            }
        }
        RegistrationRequest.retryPolicy = DefaultRetryPolicy(
            25000,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        MySingleton.getInstance(this).addToRequestQueue(RegistrationRequest)
    }

    private fun check_internet() {
        val dialogexit = Dialog(this)
        dialogexit.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogexit.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogexit.setCancelable(true)
        dialogexit.setContentView(R.layout.check_internet_dialog)
//        val retry = dialogexit.findViewById<View>(R.id.retry) as TextView
//        val exit = dialogexit.findViewById<View>(R.id.exit) as TextView
//        retry.setOnClickListener {
//            if (Utils.isInternetAvailable_for_popup(this@QuizQuestionsActivity)) {
//                dialogexit.dismiss()
//                finish()
//            } else {
//                Toast.makeText(
//                    this@QuizQuestionsActivity,
//                    "Internet not available",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//        exit.setOnClickListener {
//            dialogexit.dismiss()
//            finish()
//        }
        dialogexit.show()
    }
}