package com.machineries_pk.mrk.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.machineries_pk.mrk.databinding.ActivityHomeBinding
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.fragments.FindCustomerFragment
import com.machineries_pk.mrk.fragments.HistoryFragment
import com.machineries_pk.mrk.fragments.ReportFragment
import io.paperdb.Paper


class Home : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

//        val byteArray = intent.getByteArrayExtra("image")
//        val bmp = byteArray?.let { BitmapFactory.decodeByteArray(byteArray, 0, it.size) }

//        binding.employeeImg.setImageBitmap(bmp)

        binding.invoice.setOnClickListener {
            Paper.book().write("from_invoice",true)
            binding.invoice.setImageResource(R.drawable.invoice_selected)
            binding.invoiceBg.setBackgroundResource(R.drawable.selected_item_bg)
            binding.history.setImageResource(R.drawable.history)
            binding.historyBg.setBackgroundResource(R.drawable.unselected_item_bg)
            binding.report.setImageResource(R.drawable.report)
            binding.reportBg.setBackgroundResource(R.drawable.unselected_item_bg)

            val fm = supportFragmentManager
            fm.beginTransaction().replace(binding.framelayout.id,FindCustomerFragment()).commit()
        }

        binding.history.setOnClickListener {
            Paper.book().write("from_invoice",false)
            binding.invoice.setImageResource(R.drawable.invoice)
            binding.invoiceBg.setBackgroundResource(R.drawable.unselected_item_bg)
            binding.history.setImageResource(R.drawable.history_selected)
            binding.historyBg.setBackgroundResource(R.drawable.selected_item_bg)
            binding.report.setImageResource(R.drawable.report)
            binding.reportBg.setBackgroundResource(R.drawable.unselected_item_bg)

            val fm = supportFragmentManager
            fm.beginTransaction().replace(binding.framelayout.id,FindCustomerFragment()).commit()
        }

        binding.report.setOnClickListener {
            binding.invoice.setImageResource(R.drawable.invoice)
            binding.invoiceBg.setBackgroundResource(R.drawable.unselected_item_bg)
            binding.history.setImageResource(R.drawable.history)
            binding.historyBg.setBackgroundResource(R.drawable.unselected_item_bg)
            binding.report.setImageResource(R.drawable.report_selected)
            binding.reportBg.setBackgroundResource(R.drawable.selected_item_bg)

            val fm = supportFragmentManager
            fm.beginTransaction().replace(binding.framelayout.id,ReportFragment()).commit()
        }

    }
}