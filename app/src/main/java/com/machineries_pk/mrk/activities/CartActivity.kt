package com.machineries_pk.mrk.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.machineries_pk.mrk.R
import com.machineries_pk.mrk.Utils.MySingleton
import com.machineries_pk.mrk.Utils.Server
import com.machineries_pk.mrk.adapters.AllItemsAdapter
import com.machineries_pk.mrk.adapters.EmployeeRecyclerAdapter
import com.machineries_pk.mrk.adapters.SelectedItemsAdapter
import com.machineries_pk.mrk.databinding.ActivityCartBinding
import com.machineries_pk.mrk.databinding.FragmentReportBinding
import com.machineries_pk.mrk.models.EmployeeModel
import com.machineries_pk.mrk.models.ItemModel
import com.machineries_pk.mrk.models.SelectedItemModel
import io.paperdb.Paper
import org.json.JSONException
import org.json.JSONObject

class CartActivity : AppCompatActivity() {


    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var viewAdapterselected: RecyclerView.Adapter<*>
    private lateinit var viewManagerselected: RecyclerView.LayoutManager

    private lateinit var binding: ActivityCartBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCartBinding.inflate(layoutInflater)
        setContentView(binding.root)



       get()



        viewManagerselected = LinearLayoutManager(this)
        viewAdapterselected = SelectedItemsAdapter(this,seletectedSampleList)
        binding.selectedItemsRecyclerview.adapter = viewAdapterselected
        binding.selectedItemsRecyclerview.apply{
            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = viewManagerselected

            // specify an viewAdapter (see also next example)
            adapter = viewAdapterselected

        }

        binding.checkoutMinCheckout.setOnClickListener(View.OnClickListener {
           startActivity(Intent( this, CheckoutAdapterActivity::class.java))

        })

    }

//    private val sampleList : ArrayList<ItemModel>
     fun   get()
        {

//            itemlist.add(
//                ItemModel(
//                    BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
//                    "New Model","Oyster",500)
//            )
//            itemlist.add(
//                ItemModel(
//                    BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
//                "New Model","Oyster",500)
//            )
//            itemlist.add(
//                ItemModel(
//                    BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
//                "New Model","Oyster",500)
//            )
//            itemlist.add(
//                ItemModel(
//                    BitmapFactory.decodeResource(resources,R.drawable.khan_image),"123123","Explorer",
//                    "New Model","Oyster",500)
//            )

//            return itemlist





            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.loading_dialog)




            dialog.show()

            val RegistrationRequest: StringRequest = object : StringRequest(
                Method.GET,
                Server.baseurl +"GetItems?whseCode=01",
                Response.Listener
                { response ->
                    try {


                        val objects = JSONObject(response)
                        val response_code = objects.getInt("errorCode")
                        if (response_code == 0) {
                            if (dialog != null && dialog.isShowing()) {
                                dialog.dismiss();
                            }
                            val itemlist: ArrayList<ItemModel> = ArrayList()
                            val results = objects.getJSONArray("data")
                            for (i in 0 until results.length()) {
                                val jsonObject = results.getJSONObject(i)

                                val itemCode = jsonObject.getString("itemCode")
                                val itemName = jsonObject.getString("itemName")
                                val price = jsonObject.getInt("price")
                                val qty = jsonObject.getInt("onHand")


                                itemlist?.add(
                                    ItemModel(
                                        BitmapFactory.decodeResource(resources,R.drawable.khan_image),itemCode,itemName,
                                        "","",price)
                                )

//                                itemlist?.add(EmployeeModel(BitmapFactory.decodeResource(resources,R.drawable.zia_img),slpName,jobTitle))



                            }
                            viewManager = LinearLayoutManager(this)
                            viewAdapter = AllItemsAdapter(this,itemlist)
                            binding.recyclerview.adapter = viewAdapter



                            binding.recyclerview.apply{
                                // use this setting to improve performance if you know that changes
                                // in content do not change the layout size of the RecyclerView
                                setHasFixedSize(true)

                                // use a linear layout manager
                                layoutManager = viewManager

                                // specify an viewAdapter (see also next example)
                                adapter = viewAdapter

                            }
                        } else {
                            Toast.makeText(
                                this,
                                objects.getString("errorMessage"),
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                        //                    Toast.makeText(getActivity(), msg, Toast.LENGTH_SHORT).show();
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }
                    dialog.dismiss()
                },
                object : Response.ErrorListener {
                    override fun onErrorResponse(volleyError: VolleyError) {
                        dialog.dismiss()
                        var message: String? = null
                        check_internet()
                        when (volleyError) {
                            is NetworkError -> {
                                message = "Cannot connect to Internet...Please check your connection!"
                            }
                            is ServerError -> {
                                message =
                                    "The server could not be found. Please try again after some time!!"
                            }
                            is AuthFailureError -> {
                                message = "Cannot connect to Internet...Please check your connection!"
                            }
                            is ParseError -> {
                                message = "Parsing error! Please try again after some time!!"
                            }
                            is NoConnectionError -> {
                                message = "Cannot connect to Internet...Please check your connection!"
                            }
                            is TimeoutError -> {
                                message = "Connection TimeOut! Please check your internet connection."
                            }
                        }
                        Toast.makeText(this@CartActivity, message, Toast.LENGTH_LONG).show()
                    }
                }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val header: MutableMap<String, String> = HashMap()
                    header["Authorization"] = "bearer ${Paper.book().read("token","")}"
//                header["x-rapidapi-host"] = "twinword-word-association-quiz.p.rapidapi.com"
                    return header


                }
            }
            RegistrationRequest.retryPolicy = DefaultRetryPolicy(
                25000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
            )
            MySingleton.getInstance(this).addToRequestQueue(RegistrationRequest)








        }
    private fun check_internet() {
        val dialogexit = Dialog(this)
        dialogexit.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialogexit.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogexit.setCancelable(true)
        dialogexit.setContentView(R.layout.check_internet_dialog)
//        val retry = dialogexit.findViewById<View>(R.id.retry) as TextView
//        val exit = dialogexit.findViewById<View>(R.id.exit) as TextView
//        retry.setOnClickListener {
//            if (Utils.isInternetAvailable_for_popup(this@QuizQuestionsActivity)) {
//                dialogexit.dismiss()
//                finish()
//            } else {
//                Toast.makeText(
//                    this@QuizQuestionsActivity,
//                    "Internet not available",
//                    Toast.LENGTH_SHORT
//                ).show()
//            }
//        }
//        exit.setOnClickListener {
//            dialogexit.dismiss()
//            finish()
//        }
        dialogexit.show()
    }

    private val seletectedSampleList : ArrayList<SelectedItemModel>
    get()
    {
        val itemList: ArrayList<SelectedItemModel> = ArrayList()
        itemList.add(
            SelectedItemModel(
                BitmapFactory.decodeResource(resources,R.drawable.khan_image),"Model: 123123","Serial: 123123",3000,-500,2500
            )
        )
        itemList.add(
            SelectedItemModel(
                BitmapFactory.decodeResource(resources,R.drawable.khan_image),"Model: 124232","Serial: 45663",2000,-500,1500
            )
        )
        itemList.add(
            SelectedItemModel(
                BitmapFactory.decodeResource(resources,R.drawable.khan_image),"Model: 124335","Serial: 14433",3000,-100,2900
            )
        )

        return itemList
    }

}